Deploying Steps

local machine
cd C:\Users\jjirasirikul\Documents\BitBucket\Camerons_CW\cw-DIFOT-report
docker-compose up -d --build
docker save -o cw-difot-report_main.tar cw-difot-report_main
docker-compose stop

COPY FILE TO SERVER D:\DockerImages\cw-DIFOT-report (via T:\ Drive is fastest)

mv -Force T:\BIScript_data\cw-difot-report_main.tar .\cw-difot-report_main.tar

server (since no compose component on the server)
# Remark -  You may need to restart "Hyper-V Virtual Machine Management" service before deploy

# docker rm -f cw-push-database-script_dev
cd D:\DockerImages\cw-DIFOT-report
docker load -i .\cw-difot-report_main.tar
# docker run --platform linux/amd64 --name cw-push-database-script_dev --env-file .env\dev.env -d --restart=always cw-push-database-script_main
docker-compose down
docker-compose create

# docker-compose up -d
docker logs docker logs cw-difot-report-dev-1




