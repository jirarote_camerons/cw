#!/usr/bin/env python
# coding: utf-8

# In[21]:


import logging
import sys
import base64
import os

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

# Enable to see the log within 
log_format = '%(asctime)s  %(name)s  %(levelname)s: %(message)s'
logging.basicConfig(format=log_format, level=logging.INFO)

class SMTPconnection:
    logger = logging.getLogger('SMTPconnection')
    
    def __init__(self, username = None, password = None, host='smtp.office365.com', port=587):
        self.username = username
        self.password = password
        self.host = host
        self.port = port
        self.connection = None
    
    def __enter__(self):
        return self.open_connection()
     
    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.close_connection()
    
    ###################  CONNECTION MANAGER  ###########################
    def open_connection(self):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {}".format(func_name))
        try:
            self.logger.info("Connecting to STMP: {}:{}".format(self.host, self.port))
            
            # create the connection
            connection = smtplib.SMTP(host='smtp.office365.com', port=587)
            connection.starttls()
            connection.login(self.username, 
                             self.password)
            self.logger.info('SMTP connected!')
            self.connection = connection
            return self
        except Exception as e:
            self.logger.error('SMTP connection failed...')
            raise Exception(e)
            
    def close_connection(self):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {}".format(func_name))

        try:
            self.connection.quit()
            self.logger.info('SMTP disconnected!')
        except Exception as e:
            self.logger.error('SMTP disconnection failed...')
            raise Exception(e)    
            
    ###################  CONNECTION MANAGER  ###########################
    
    ###################  SMTP UTILS  ###########################
    def send_once(self, mimemsg):
        self.connection.send_message(mimemsg)
        self.logger.info('Email Sent!')
    
    ###################  SMTP UTILS  ###########################
    
    


# In[22]:


class MIMEBuilder:
#     logger = logging.getLogger('MIMEBuilder')
    
    def email_list_string(input_list):
        return ', '.join(input_list)
    
    def create_mime(mail_from, mail_to = None, mail_subject = "Example Email" ,mail_body = "Placeholder", mail_cc = None, mail_bcc = None):
        mimemsg = MIMEMultipart()
        mimemsg['From']= mail_from
        mimemsg['Subject']= mail_subject
        mimemsg.attach(MIMEText(mail_body, 'html'))
        
        if(mail_to != None):
            mimemsg['To']= mail_to
        if(mail_cc != None):
            mimemsg['Cc']= mail_cc
        if(mail_bcc != None):
            mimemsg["Bcc"] = mail_bcc
        
        return mimemsg
    
    # locate and attach desired attachments
    def add_attachment(mimemsg, set_att_name, att_source_path):

        # locate and attach desired attachments
        try:
            att_name = os.path.basename(set_att_name)
            _f = open(att_source_path, 'rb')
            att = MIMEApplication(_f.read(), _subtype="txt")
            _f.close()
            att.add_header('Content-Disposition', 'attachment', filename=att_name)
            mimemsg.attach(att)
        except Exception as e:
            raise Exception(e)   


# In[23]:


if __name__ == "__main__":
    print ("Executed when invoked directly")
    
    email_username = "Camerons.bi.prod@camerons.com.au"
    email_password = ""
    
    with SMTPconnection(email_username,base64.b64decode(email_password).decode("utf-8")) as my_smtp:
    
        mimemsg = MIMEBuilder.create_mime(email_username,
                                          MIMEBuilder.email_list_string(["jirarote.jirasirikul@camerons.com.au"])
                                            )
        my_smtp.send_once(mimemsg)


# In[ ]:





# In[ ]:




