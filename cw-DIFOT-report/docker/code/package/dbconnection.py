#!/usr/bin/env python
# coding: utf-8

# In[1]:


# !pip install pandas


# In[2]:


#!pip install pyodbc


# In[3]:


import logging
import sqlalchemy as sa
import sys
import pandas as pd

# Enable to see the log within 
# log_format = '%(asctime)s  %(name)s  %(levelname)s: %(message)s'
# logging.basicConfig(format=log_format, level=logging.DEBUG)


class dbconnection:
    logger = logging.getLogger('dbconnection')
    
    
    def __init__(self, 
                 servername, database = 'MASTER', 
                 username = None, password = None,
                 dbdriver = 'ODBC Driver 11 for SQL Server'):
        self.servername = servername
        self.database = database
        self.username = username
        self.password = password
        self.dbdriver = dbdriver
        self.engine = None
    
         
    def __enter__(self):
        return self.open_sqlconnection()
     
    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.close_sqlconnection()
    

    ###################  CONNECTION MANAGER  ###########################
    def open_sqlconnection(self):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {}".format(func_name))
        try:
            self.logger.debug("Connecting to: [{}].[{}]".format(self.servername, self.database))
            # create the connection
            if(self.username == None):
                self.logger.debug("Database Authen: Windows Authentication")
                connection_uri = sa.engine.URL.create(
                    "mssql+pyodbc",
                    host = self.servername,
                    database = self.database,
                    query={"driver": self.dbdriver },
                )
            else:

                self.logger.debug("Database Authen: SQL Server Authentication")
                connection_uri = sa.engine.URL.create(
                    "mssql+pyodbc",
                    username = self.username,
                    password = self.password,
                    host = self.servername,
                    database = self.database,
                    query={"driver": self.dbdriver },
                )
    
            str_logging = "Database [{servername}].[{database}] using {auth_type}".format(
                servername = self.servername,
                database = self.database,
                auth_type = "Windows Authentication" if(self.username == None) else "SQL Server Authentication ({})".format(self.username)
            )
    
            engine = sa.create_engine(connection_uri,connect_args = {"TrustServerCertificate": "yes"})
            res = engine.connect()
            self.logger.info(str_logging + " connected!")
            self.engine = engine
            return self
        except Exception as e:
            self.logger.error(str_logging + " NOT connected!")
            self.logger.debug(e)
            
            raise Exception(e)
            
    def close_sqlconnection(self):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {}".format(func_name))

        str_logging = "Database [{servername}].[{database}] using {auth_type}".format(
                servername = self.servername,
                database = self.database,
                auth_type = "Windows Authentication" if(self.username == None) else "SQL Server Authentication ({})".format(self.username)
        )
        
        try:
            if(self.engine != None):
                self.engine.dispose()
                self.engine = None
            self.logger.debug(str_logging + ' disconnected!')
        except Exception as e:
            self.logger.error(str_logging + ' disconnection failed...')
            self.logger.debug(e)
            
            raise Exception(e)    
            
    ###################  CONNECTION MANAGER  ###########################
    
    ###################  DATABASE QUERY  ###########################
    def is_db_table_existed(self, table_name, schema = 'dbo'):
        isTable = sa.inspect(self.engine).has_table(table_name, schema = schema)  
        self.logger.debug("Table '[{}].[{}].[{}].[{}]' {}".format(
            self.servername, self.database, schema, table_name, 'existed' if isTable else 'does not exist'))
        return isTable
    
    def read_to_df(self, table_name, schema = 'dbo', n = None):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {}".format(func_name))

        sql = """
            SELECT {} *
                FROM [{}].[{}]
        """.format("" if n == None else "TOP ({})".format(n), schema, table_name)
            
        try:
            self.logger.debug('====== SQL ======')
            self.logger.debug(sql)
            self.logger.debug('=================')
            query_df = pd.read_sql(sql, self.engine)
            self.logger.debug('Read Success with df size: {}'.format(query_df.shape))
            return query_df
        except Exception as e:
            self.logger.error('Read failed...')
#             raise Exception(e)  
            
    def write_from_df(self, table_name, input_df, schema = 'dbo', is_replace = False, is_admin = False):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {} {}".format(func_name,table_name))
    
        if(self.is_db_table_existed(table_name , schema)):
            try:
                if(is_replace):
                    logger.debug("Truncate Table: [{}].[{}]".format(schema, table_name))
                    # If table exist -> Truncate before add value
                    Session = sa_orm.sessionmaker(bind=self.engine)
                    session = Session()
                    session.execute('''TRUNCATE TABLE [{}].[{}]'''.format(schema, table_name))
                    session.commit()
                    session.close()
                    self.logger.debug('Truncate Table success')
                
                input_df.to_sql(table_name, index=False, con=self.engine, schema = schema, if_exists="append")
                self.logger.debug('Write data to table success')
            except Exception as e:
                self.logger.error('Write data to table failed')
                raise Exception(e)  
        else:
            if(is_admin):
                self.logger.warning("Creating new table should be done manually...")
                self.logger.warning("OVERRIDE Creating table as ADMIN")
                try:
                    input_df.to_sql(table_name, index=False, con=self.engine, schema = schema, if_exists="append")
                    self.logger.debug('Creating new table success')
                    self.logger.debug('Write data to table success')
                except Exception as e:
                    self.logger.error('Creating new table failed')
                    raise Exception(e)                       
            else:
                self.logger.error("Write data to table failed. Table does not exist")
    
    def read_sql(self, sql_script_string):
        try:
            self.logger.debug('====== SQL ======')
            self.logger.debug(sql_script_string)
            self.logger.debug('=================')
            query_df = pd.read_sql(sql_script_string,self.engine) 
            self.logger.debug('Read Success with df size: {}'.format(query_df.shape))
            return query_df
        except Exception as e:
            self.logger.error('Read failed...')
    
    def read_sql_file(self, script_file_path):
        try:
            # Open the external sql file.
            file = open(script_file_path, 'r')

            # Read out the sql script text in the file.
            sql_script_string = file.read()

            # Close the sql file object.
            file.close()
            self.logger.debug('Open SQL file success')
            
            return self.read_sql(sql_script_string)
        except Exception as e:
            self.logger.error('Open SQL file failed...')
            

    def execute_sql(self, sql_script_string):
        try:
            self.logger.debug('====== SQL ======')
            self.logger.debug(sql_script_string)
            self.logger.debug('=================')
            Session = sa_orm.sessionmaker(bind=self.engine)
            session = Session()
            session.execute(sql_script_string)
            session.commit()
            session.close()
            self.logger.debug('Execute SQL success')
        except Exception as e:
            self.logger.error('Execute SQL failed...')
            
    def execute_sql_file(self, script_file_path):
        try:
            # Open the external sql file.
            file = open(script_file_path, 'r')

            # Read out the sql script text in the file.
            sql_script_string = file.read()

            # Close the sql file object.
            file.close()
            self.logger.debug('Open SQL file success')
            
            self.execute_sql(sql_script_string)
        except Exception as e:
            self.logger.error('Open SQL file failed...')

    ###################  DATABASE QUERY  ###########################
    
    


# In[4]:


# Testing Class
if __name__ == "__main__":
    print ("Executed when invoked directly")

    with dbconnection('VSDEVODS01','BI', dbdriver= "ODBC Driver 18 for SQL Server") as test_engine:
        temp_df = test_engine.read_to_df('script_execution','LOG', n = 500)
        
        timestamp_df = pd.DataFrame([{"script":'JJ_Test',
                                      "refresh_time":pd.Timestamp.today(),
                                      "status": "Success"}])
        
        test_engine.write_from_df("script_execution", timestamp_df, schema = "LOG")
        
    print(temp_df)  


# In[ ]:




