SELECT 
	[dispatch_id]
      ,[leg_number]
      ,[leg_type]
      ,[leg_id]
      ,[route_id]
      ,[dispatch_created_utc]
      ,[driver_id]
      ,[vehicle_id]
      ,[target_customer]
      ,[dl_tote]
      ,[arrival_datetime_utc]
      ,[start_action_datetime_utc]
      ,[end_action_datetime_utc]
      ,[depart_datetime_utc]
      ,[leg_status]
      ,[last_modified_datetime_utc]
      ,[driver_displayname]
      ,[vehicle_name]
      ,[customer_name]
      ,[customer_eqcode]
      ,[customer_state]
      ,[customer_latitude]
      ,[customer_longitude]
      ,[customer_window_open_tz]
      ,[customer_window_close_tz]
  FROM [BI].[CW].[view_leg_status]
  WHERE  (
			-- GPS time is converted to local time by getting the difference in hours between the current UTC time and the current local time
			-- This isn't historically precise and will be up to 1 hour off on daylight savings changes but the potential for difference
			-- will last only 1 day at the two DST time changes during a year.
			-- SQL 2016 introduced an AT TIMEZONE function to adjust a historical date from one timezone to another
			-- Until this query can be run on a newer version of SQL Server, this imprecise approach is used
			-- In addition, this does not currently take into account the timezone where the action was performed
			arrival_datetime_utc >= DATEADD(hh, 14 ,DATEADD(day, DATEDIFF(day, 0, GETDATE()) - 1, -3)) 
			AND arrival_datetime_utc < DATEADD(hh, 14 ,DATEADD(day, DATEDIFF(day, 0, GETDATE()) - 0, -3))
			)
			AND leg_status = 'Delivered'
	ORDER BY arrival_datetime_utc