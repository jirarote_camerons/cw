USE [BI]
GO

/****** Object:  View [MTDATA].[vehicles_cw]    Script Date: 12/05/2022 3:06:33 PM ******/
DROP VIEW [MTDATA].[vehicles_cw]
GO

/****** Object:  View [MTDATA].[vehicles_cw]    Script Date: 12/05/2022 3:06:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [MTDATA].[vehicles_cw]  
AS  
(SELECT 
	[id] as 'vehicle_id',
	[FleetID] as 'fleet_id',
	[DisplayName] as 'vehicle_name',
	CASE
		WHEN [DisplayName] LIKE 'CW-NSW%' THEN 'NSW'
		WHEN [DisplayName] LIKE 'CW-QLD%' THEN 'QLD'
		WHEN [DisplayName] LIKE 'CW-SA%' THEN 'SA'
		WHEN [DisplayName] LIKE 'CW-WA%' THEN 'WA'
		WHEN [DisplayName] LIKE 'CW-VIC%' THEN 'VIC'
		WHEN [DisplayName] LIKE 'CW-NT%' THEN 'NT'
		WHEN [DisplayName] LIKE 'CW-TAS%' THEN 'TAS'
		ELSE 'UNK'
	END AS 'state'
  FROM [BI].[dbo].[vehicles]
  WHERE [FleetID] IN ('91038','91039','91040'))
GO


