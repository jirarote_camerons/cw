/****** Script for SelectTopNRows command from SSMS  ******/
SELECT
	max_dp.dispatch_id,
	(SELECT MAX(LastUpdateDate)
      FROM (VALUES (max_arrival),(max_start_unload),(max_end_unload),(max_depart)) AS UpdateDate(LastUpdateDate)) 
	  AS 'last_modify_datetime_utc'
FROM
(
SELECT
	dispatch_id,
	MAX(arrival_datetime_utc) as 'max_arrival',
	MAX(start_unload_datetime_utc)  as 'max_start_unload',
	MAX(end_unload_datetime_utc)  as 'max_end_unload',
	MAX(depart_datetime_utc)  as 'max_depart'
  FROM [BI].[CW].[view_leg_status]
  WHERE [job_status] NOT IN ('Complete','Cancelled','Fail')
  GROUP BY dispatch_id) as max_dp