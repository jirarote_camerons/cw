/****** Script for SelectTopNRows command from SSMS  ******/

SELECT COALESCE(b.[State],'UNK') as 'State',b.[FleetID],a.dispatchId,a.jobStatus,a.gpstime,a.[driver.id],a.[vehicle.id]
FROM
	(
	SELECT r.[dispatchId],[jobStatus],[gpstime],dd.[driver.id],dd.[vehicle.id]
	FROM
	(
		SELECT [dispatchId],MAX(eventId) as MaxEvent
		  FROM [PIP].[MTDATA].[dispatch_events]
		  WHERE [eventType] = 'JobStatusChanged'
		  GROUP BY [dispatchId]) r
	INNER JOIN [PIP].[MTDATA].[dispatch_events] dd
	ON dd.[dispatchId] = r.[dispatchId] AND dd.eventId = r.MaxEvent) a
LEFT JOIN [PIP].[MTDATA].[vehicles_cw] b
ON a.[vehicle.id] = b.[VehicleID];


SELECT 
	COALESCE(veh.[State],'UNK') as 'state',
	dp.[dispatchId] as 'dispatch_id',
	min_event.[gpstime] as 'dispatch_created_utc', /*** Assume First Event is Created ***/
	max_event.[fleetId] as 'fleet_id',
	max_event.[jobStatus] as 'job_status',
	max_event.[gpstime] as 'dispatch_modified_utc', /*** Assume Last Event is Last Modified ***/
	veh.[VehicleID] as 'vehicle_id',
	veh.[DisplayName] as 'vehicle_name',
	emp.[EDKey],
	emp.[firstname] as 'driver_firstname',
	emp.[lastname] as 'driver_lastname',
	CASE 
	WHEN emp.[EDKey] IS NULL THEN NULL
	WHEN emp.[lastname] IS NULL THEN CONCAT(UPPER(LEFT(emp.[firstname],1)),LOWER(SUBSTRING(emp.[firstname],2,LEN(emp.[firstname]))))
	ELSE CONCAT(UPPER(LEFT(emp.[firstname],1)),LOWER(SUBSTRING(emp.[firstname],2,LEN(emp.[firstname]))),' ',UPPER(SUBSTRING(emp.[lastname],1,1)),'.') END
	as 'driver_shortname',
	emp.[costcenter]
FROM
	(SELECT [dispatchId],MAX(eventId) as MaxEvent, MIN(eventId) as MinEvent
	FROM [PIP].[MTDATA].[dispatch_events] 
	WHERE [eventType] = 'JobStatusChanged'
	GROUP BY [dispatchId]) dp
LEFT JOIN [PIP].[MTDATA].[dispatch_events] max_event
ON dp.[dispatchId] = max_event.[dispatchId] AND dp.MaxEvent = max_event.eventId
LEFT JOIN [PIP].[MTDATA].[dispatch_events] min_event
ON dp.[dispatchId] = min_event.[dispatchId] AND dp.MinEvent = min_event.eventId
LEFT JOIN [PIP].[MTDATA].[vehicles_cw] veh
ON max_event.[vehicle.id] = veh.[VehicleID]
LEFT JOIN [PIP].[dbo].[View_ED_Employee] emp
ON max_event.[driver.id] = emp.[MTD.id]
ORDER BY dp.[dispatchId] desc