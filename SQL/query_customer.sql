/****** Script for SelectTopNRows command from SSMS  ******/
SELECT DISTINCT *
FROM (
SELECT
      [delivery_customer]
      ,[delivery_customer_address]
      ,[delivery_customer_phone]
  FROM [JITTERBIT_EVENTSTORE].[SAPPHIRE].[runs_xref]) as a
 WHERE a.[delivery_customer] != ''