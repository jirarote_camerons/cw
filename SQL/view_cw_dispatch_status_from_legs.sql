USE [BI]
GO

/****** Object:  View [CW].[view_dispatch_status_from_legs]    Script Date: 12/05/2022 3:04:45 PM ******/
DROP VIEW [CW].[view_dispatch_status_from_legs]
GO

/****** Object:  View [CW].[view_dispatch_status_from_legs]    Script Date: 12/05/2022 3:04:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/****** Script for SelectTopNRows command from SSMS  ******/
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [CW].[view_dispatch_status_from_legs]
AS
(
SELECT 
	*,
	CASE
		WHEN [count_legs] = [count_legs_departed] THEN 'Completed'
		WHEN [last_modified_datetime_utc] IS NOT NULL THEN 'Leg In Progress'
		ELSE 'Not Started'
	END as 'dispatch_status_from_legs'
FROM
(
SELECT 
	leg_dp.[dispatch_id],
	(SELECT COUNT([leg_number]) FROM [BI].[CW].[view_leg_status] a1 WHERE leg_dp.[dispatch_id] = a1.[dispatch_id]) as 'count_legs',
	(SELECT COUNT([leg_number]) FROM [BI].[CW].[view_leg_status] a1 WHERE leg_dp.[dispatch_id] = a1.[dispatch_id] AND a1.[leg_status] = 'Departed') as 'count_legs_departed',
	MAX(leg_dp.[last_modified_datetime_utc]) as 'last_modified_datetime_utc'
FROM [BI].[CW].[view_leg_status] leg_dp
GROUP BY leg_dp.[dispatch_id]) temp
);
GO


