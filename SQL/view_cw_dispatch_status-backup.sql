USE [BI]
GO

/****** Object:  View [CW].[view_dispatch_status]    Script Date: 12/05/2022 3:03:50 PM ******/
DROP VIEW [CW].[view_dispatch_status]
GO

/****** Object:  View [CW].[view_dispatch_status]    Script Date: 12/05/2022 3:03:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/****** Script for SelectTopNRows command from SSMS  ******/
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [CW].[view_dispatch_status]
AS
(
SELECT 
	dp.[dispatchId] as 'dispatch_id',
	min_event.[gpstime] as 'dispatch_created_utc', /*** Assume First Event is Created ***/
	max_event.[fleetId] as 'fleet_id',
	CASE
		WHEN max_event.[jobStatus] = 'Complete' THEN 'Completed'
		WHEN max_event.[jobStatus] = 'NotStarted' THEN 'Not Started'
		ELSE max_event.[jobStatus]
	END as 'dispatch_status',
	max_event.[gpstime] as 'dispatch_modified_utc', /*** Assume Last Event is Last Modified ***/
	max_event.[driver.id] as 'driver_id',
	max_event.[vehicle.id] as 'vehicle_id'
FROM
	(SELECT [dispatchId],MAX(eventId) as MaxEvent, MIN(eventId) as MinEvent
	FROM [BI].[MTDATA].[dispatch_events] 
	WHERE [eventType] = 'JobStatusChanged'
	GROUP BY [dispatchId]) dp
LEFT JOIN [BI].[MTDATA].[dispatch_events] max_event
ON dp.[dispatchId] = max_event.[dispatchId] AND dp.MaxEvent = max_event.eventId
LEFT JOIN [BI].[MTDATA].[dispatch_events] min_event
ON dp.[dispatchId] = min_event.[dispatchId] AND dp.MinEvent = min_event.eventId
);
GO


