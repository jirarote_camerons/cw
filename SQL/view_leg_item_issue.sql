USE [BI]
GO

/****** Object:  View [CW].[view_leg_item_issue]    Script Date: 12/05/2022 3:05:24 PM ******/
DROP VIEW [CW].[view_leg_item_issue]
GO

/****** Object:  View [CW].[view_leg_item_issue]    Script Date: 12/05/2022 3:05:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/****** Script for SelectTopNRows command from SSMS  ******/
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [CW].[view_leg_item_issue]
AS
(
SELECT 
	dp_issue.[dispatch_id],
	dp_legs.[leg_id],
	[item_name] as 'item_barcode',
	CASE 
		WHEN leg_items.[item_count] > 0 then 1
		ELSE 0 
	END as 'item_is_scanned',
	CASE
		WHEN LEFT([item_name],2) = 'OB' then 'Pallet'
		WHEN LEFT([item_name],1) = 'C' then 'Sitecode'
		WHEN LEFT([item_name],5) = '00004' then 'Tote'
		ELSE 'unknown'
	END as 'item_type'
FROM
(
	SELECT 
		DISTINCT [dispatch_id]
	FROM [BI].[CW].[view_leg_item_scanned]
	WHERE [item_count] <> [scan_count]) as dp_issue
LEFT JOIN [BI].[CW].[view_leg_item_scanned] as dp_legs
ON dp_issue.[dispatch_id] = dp_legs.[dispatch_id]
LEFT JOIN [BI].[dbo].[CW_Leg_Complete_Action_Items] as leg_items
ON dp_legs.leg_id = leg_items.leg_id
);
GO


