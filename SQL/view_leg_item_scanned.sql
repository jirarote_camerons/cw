USE [BI]
GO

/****** Object:  View [CW].[view_leg_item_scanned]    Script Date: 12/05/2022 3:05:43 PM ******/
DROP VIEW [CW].[view_leg_item_scanned]
GO

/****** Object:  View [CW].[view_leg_item_scanned]    Script Date: 12/05/2022 3:05:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/****** Script for SelectTopNRows command from SSMS  ******/
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [CW].[view_leg_item_scanned]
AS
(
SELECT 
	[dl_legs].dispatch_id,
	[dl_legs].[leg_id],
	[dl_legs].[item_count],
	scan.[scan_count]
FROM
(
	SELECT 
		[mtdata_dispatch_id] as 'dispatch_id',
		[leg_id],
		[item_count]
	  FROM [BI].[dbo].[runs_xref]
	  WHERE [leg_type] = 'DL'
	  AND [leg_id] IS NOT NULL
) as dl_legs
LEFT JOIN 
(
	SELECT 
		[leg_id], 
		COUNT(*) as 'scan_count'
	FROM [BI].[dbo].[CW_Leg_Complete_Action_Items]
	WHERE [item_name] LIKE '00004%'
	AND [item_count] <> 0
	GROUP BY [leg_id]
) as scan
ON dl_legs.[leg_id] = scan.[leg_id]
);
GO


