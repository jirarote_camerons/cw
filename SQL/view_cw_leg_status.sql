USE [BI]
GO

/****** Object:  View [CW].[view_leg_status]    Script Date: 12/05/2022 3:06:00 PM ******/
DROP VIEW [CW].[view_leg_status]
GO

/****** Object:  View [CW].[view_leg_status]    Script Date: 12/05/2022 3:06:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








/****** Script for SelectTopNRows command from SSMS  ******/
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [CW].[view_leg_status]
AS
(
SELECT 
	all_legs.*,
	dp_status.[driver_id],
	dp_status.[vehicle_id],
	e_arrive.[gps_time] as 'arrival_datetime_utc',
	e_unload_start.[gps_time] as 'start_unload_datetime_utc',
	e_unload_end.[gps_time] as 'end_unload_datetime_utc',
	e_depart.[gps_time] as 'depart_datetime_utc',
	CASE
		WHEN e_depart.[gps_time] IS NOT NULL THEN 'Departed'
		WHEN e_unload_end.[gps_time] IS NOT NULL THEN 'Unloaded'
		WHEN e_unload_start.[gps_time] IS NOT NULL THEN 'Unloading'
		WHEN e_arrive.[gps_time] IS NOT NULL THEN 'Arrived'
		ELSE 'Not Started'
	END as 'leg_status',
	CASE
		WHEN e_depart.[gps_time] IS NOT NULL THEN e_depart.[gps_time]
		WHEN e_unload_end.[gps_time] IS NOT NULL THEN e_unload_end.[gps_time]
		WHEN e_unload_start.[gps_time] IS NOT NULL THEN e_unload_start.[gps_time]
		WHEN e_arrive.[gps_time] IS NOT NULL THEN e_arrive.[gps_time]
		ELSE NULL
	END as 'last_modified_datetime_utc'
FROM
	(
	SELECT 
		[mtdata_dispatch_id] as 'dispatch_id',
		[leg_number],
		[leg_type],
		[leg_id],
		[route_id],
		CASE 
			WHEN [leg_type] = 'PU' THEN [pickup_customer]
			WHEN [leg_type] = 'DL' THEN [delivery_customer]
			ELSE NULL
		END as 'target_customer',
		CASE
			WHEN [leg_type] = 'DL' AND [item] = 'TOTE' THEN [item_count]
			ELSE NULL
		END as 'dl_tote'
	FROM [BI].[dbo].[runs_xref]
	WHERE [mtdata_dispatch_id] IS NOT NULL) as all_legs
LEFT JOIN [BI].[CW].[view_dispatch_status] dp_status
ON all_legs.dispatch_id = dp_status.dispatch_id
LEFT JOIN [BI].[dbo].[events_site_arrive] e_arrive
ON all_legs.[leg_id] = e_arrive.[leg_id]
LEFT JOIN [BI].[dbo].[CW_Leg_Start_Actions] e_unload_start
ON all_legs.[leg_id] = e_unload_start.[leg_id]
LEFT JOIN [BI].[dbo].[CW_Leg_Complete] e_unload_end
ON all_legs.[leg_id] = e_unload_end.[leg_id]
LEFT JOIN [BI].[dbo].[events_site_depart] e_depart
ON all_legs.[leg_id] = e_depart.[leg_id]
);
GO


