USE [BI]
GO

/****** Object:  View [CW].[view_dispatch_status_full]    Script Date: 12/05/2022 3:05:08 PM ******/
DROP VIEW [CW].[view_dispatch_status_full]
GO

/****** Object:  View [CW].[view_dispatch_status_full]    Script Date: 12/05/2022 3:05:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






/****** Script for SelectTopNRows command from SSMS  ******/
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [CW].[view_dispatch_status_full]
AS
(
SELECT
	veh.[state],
	dp1.[dispatch_id],
	dp1.[dispatch_created_utc],
	dp1.[fleet_id],
	CASE
		WHEN dp1.[dispatch_status] IN ('Completed','Cancelled','Fail') THEN [dispatch_status] 
		ELSE dp2.[dispatch_status_from_legs]
	END as 'dispatch_status',
	COALESCE ([last_modified_datetime_utc],[dispatch_modified_utc]) as 'dispatch_modified_utc',
	dp1.[vehicle_id],
	veh.[vehicle_name],
	emp.[EDKey],
	dp1.[driver_id],
	CASE 
			WHEN emp.[EDKey] IS NULL THEN NULL
			WHEN emp.[lastname] IS NULL THEN CONCAT(UPPER(LEFT(emp.[firstname],1)),LOWER(SUBSTRING(emp.[firstname],2,LEN(emp.[firstname]))))
			ELSE CONCAT(UPPER(LEFT(emp.[firstname],1)),LOWER(SUBSTRING(emp.[firstname],2,LEN(emp.[firstname]))),' ',UPPER(SUBSTRING(emp.[lastname],1,1)),'.') END
		as 'driver_shortname',
	emp.[costcenter]
FROM [BI].[CW].[view_dispatch_status] dp1
LEFT JOIN [BI].[CW].[view_dispatch_status_from_legs] dp2
ON dp1.dispatch_id = dp2.dispatch_id
LEFT JOIN [BI].[MTDATA].[vehicles_cw] veh
ON dp1.[vehicle_id] = veh.[vehicle_id]
LEFT JOIN [BI].[dbo].[View_ED_Employee] emp
ON dp1.[driver_id] = emp.[MTD.id]
);
GO


