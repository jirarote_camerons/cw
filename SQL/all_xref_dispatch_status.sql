/****** Script for SelectTopNRows command from SSMS  ******/
SELECT * 
FROM
	(SELECT DISTINCT mtdata_dispatch_id
	FROM [BI].[dbo].[runs_xref]
	WHERE [mtdata_dispatch_id] IS NOT NULL) as dp_all
  LEFT JOIN 
	(SELECT *
	FROM [BI].[CW].[view_dispatch_status]) as dp_status
  ON dp_all.[mtdata_dispatch_id] = dp_status.[dispatchId]
  WHERE [jobStatus] IS NULL
  OR [jobStatus] NOT IN ('Complete','Cancelled','Fail')