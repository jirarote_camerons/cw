USE [BI]
GO

/****** Object:  View [CW].[view_dispatch_status]    Script Date: 12/04/2022 9:50:29 AM ******/
DROP VIEW [CW].[view_leg_last_modified]
GO

/****** Object:  View [CW].[view_dispatch_status]    Script Date: 12/04/2022 9:50:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [CW].[view_leg_last_modified]
AS
(
SELECT [dispatch_id], MAX([max_datetime]) as 'leg_modified_datetime_utc'
FROM
 (SELECT [dispatch_id],MAX([gps_time]) as 'max_datetime'
 FROM [BI].[dbo].[events_site_arrive] 
 GROUP BY [dispatch_id] 
 UNION
 SELECT [dispatch_id], MAX([gps_time]) as 'max_datetime'
 FROM [BI].[dbo].[CW_Leg_Start_Actions] 
 GROUP BY [dispatch_id]
 UNION
 SELECT [dispatch_id], MAX([gps_time]) as 'max_datetime'
 FROM [BI].[dbo].[CW_Leg_Complete]
 GROUP BY [dispatch_id]
 UNION
 SELECT [dispatch_id], MAX([gps_time]) as 'max_datetime'
 FROM [BI].[dbo].[events_site_depart]
 GROUP BY [dispatch_id]) t
 GROUP BY [dispatch_id]
);
GO


