USE [BI]
GO

/****** Object:  View [CW].[view_leg_status_full]    Script Date: 12/05/2022 3:06:16 PM ******/
DROP VIEW [CW].[view_leg_status_full]
GO

/****** Object:  View [CW].[view_leg_status_full]    Script Date: 12/05/2022 3:06:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








/****** Script for SelectTopNRows command from SSMS  ******/
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [CW].[view_leg_status_full]
AS
(
SELECT 
	[dispatch_id]
      ,[leg_number]
      ,[leg_type]
      ,[leg_id]
      ,[route_id]
      ,[target_customer]
      ,[dl_tote]
	  ,[EDKey]
	  ,[driver_id]
	  ,CASE 
			WHEN emp.[EDKey] IS NULL THEN NULL
			WHEN emp.[lastname] IS NULL THEN CONCAT(UPPER(LEFT(emp.[firstname],1)),LOWER(SUBSTRING(emp.[firstname],2,LEN(emp.[firstname]))))
			ELSE CONCAT(UPPER(LEFT(emp.[firstname],1)),LOWER(SUBSTRING(emp.[firstname],2,LEN(emp.[firstname]))),' ',UPPER(SUBSTRING(emp.[lastname],1,1)),'.') END
		as 'driver_shortname'
      ,veh.[vehicle_id]
	  ,veh.[vehicle_name]
      ,[arrival_datetime_utc]
      ,[start_unload_datetime_utc]
      ,[end_unload_datetime_utc]
      ,[depart_datetime_utc],
	cust.[Customer_Name] as 'customer_name',
	cust.[EqCode] as 'customer_eqcode',
	cust.[State] as 'customer_state',
	cust.[Latitude] as 'customer_latitude',
	cust.[Longitude] as 'customer_longitude',
	PARSE(LEFT(FORMAT(cust.[Open1], '0000'),2) + ':' + RIGHT(FORMAT(cust.[Open1], '0000'),2) AS TIME) as 'customer_window_open_tz',
	PARSE(LEFT(FORMAT(cust.[Close1], '0000'),2) + ':' + RIGHT(FORMAT(cust.[Close1], '0000'),2) AS TIME) as 'customer_window_close_tz'
      ,[leg_status]
	  ,[last_modified_datetime_utc]
FROM [BI].[CW].[view_leg_status] leg_status
LEFT JOIN [BI].[MTDATA].[vehicles_cw] veh
ON leg_status.[vehicle_id] = veh.[vehicle_id]
LEFT JOIN [BI].[dbo].[View_ED_Employee] emp
ON leg_status.[driver_id] = emp.[MTD.id]
LEFT JOIN [BI].[SHAREPOINT].[customers_cw] cust
ON leg_status.[target_customer] = cust.[Sitecode]
);
GO


