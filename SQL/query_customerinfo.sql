/****** Script for SelectTopNRows command from SSMS  ******/
SELECT DISTINCT *
FROM (
SELECT 
      [leg_customer_group_id]
      ,[leg_customer_group_version]
      ,[leg_customer_id]
      ,[leg_location_name]
      ,[leg_address]
      ,[leg_latitude]
      ,[leg_longitude]
  FROM [MTDATA_KINESIS].[dbo].[CW_Leg_Complete]) as a