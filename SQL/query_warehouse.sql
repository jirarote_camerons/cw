/****** Script for SelectTopNRows command from SSMS  ******/
SELECT DISTINCT *
FROM (
SELECT
      [pickup_customer]
      ,[pickup_customer_address]
      ,[pickup_customer_phone]
  FROM [JITTERBIT_EVENTSTORE].[SAPPHIRE].[runs_xref]) as a
 WHERE a.pickup_customer != ''