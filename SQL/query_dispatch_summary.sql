/****** Script for SelectTopNRows command from SSMS  ******/
SELECT 
	[mtdata_dispatch_id],
	[total_leg],
	[ref_visited],
	[leg_com_visited],
	case
		when total_leg = ref_visited AND total_leg = leg_com_visited then 'Completed'
		when ref_visited = 0 then 'Not Start'
		when ref_visited >= leg_com_visited then 'In Progress'
		else 'N/A'
	end AS 'Status',
	[Lastest leg complete time]
FROM
(SELECT
		[mtdata_dispatch_id],
		COUNT(*) AS 'total_leg',
		SUM(case when ref_leg_id is not null then 1 else 0 end) AS ref_visited, 
		SUM(case when leg_com_leg_id is not null then 1 else 0 end) AS leg_com_visited
	FROM (SELECT 
		[mtdata_dispatch_id] AS 'mtdata_dispatch_id',
		ref.[leg_id] AS 'ref_leg_id',
		leg_com.[leg_id] AS 'leg_com_leg_id'
	  FROM [JITTERBIT_EVENTSTORE].[SAPPHIRE].[runs_xref] ref
	  LEFT JOIN [MTDATA_KINESIS].[dbo].[CW_LEG_COMPLETE] leg_com
	  ON ref.[mtdata_dispatch_id] = leg_com.[dispatch_id]
	  AND ref.leg_number = leg_com.leg_number) ref_leg
	GROUP BY [mtdata_dispatch_id]) ref_leg2
LEFT JOIN (
	SELECT [dispatch_id],
			MAX(gps_time) AS 'Lastest leg complete time'
	  FROM [MTDATA_KINESIS].[dbo].[CW_Leg_Complete]
	  GROUP BY [dispatch_id]) com_time
	ON ref_leg2.mtdata_dispatch_id = com_time.dispatch_id
ORDER BY [mtdata_dispatch_id] DESC
