SELECT 
		[mtdata_dispatch_id] AS 'mtdata_dispatch_id',
		ref.[leg_id] AS 'ref_leg_id',
		leg_com.[leg_id] AS 'leg_com_leg_id',
		*
	  FROM [JITTERBIT_EVENTSTORE].[SAPPHIRE].[runs_xref] ref
	  LEFT JOIN [MTDATA_KINESIS].[dbo].[CW_LEG_COMPLETE] leg_com
	  ON ref.[mtdata_dispatch_id] = leg_com.[dispatch_id]
	  AND ref.leg_number = leg_com.leg_number
WHERE [mtdata_dispatch_id] = '643218'
