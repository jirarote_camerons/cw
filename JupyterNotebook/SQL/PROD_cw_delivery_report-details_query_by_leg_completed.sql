-- CW Delivery Exception Report - Details Query by Leg
WITH
cte_ytd_dispatch AS (
	SELECT 
		lc.dispatch_id																				-- MTData Dispatch Id is the primary key for a job in MTData
		,lc.sequence_id																				-- Kinesis Sequence Id which links all CW records across tables that were populate from a distinct Kinesis event, this can be used to find the email alert that was sent out
		,lc.leg_location_name
		,lc.leg_title
		,runs_xref.delivery_customer
		,spjobs.col_state
		,spjobs.col_code
		,spjobs.col_name
		,spjobs.del_state
		,spjobs.del_code
		,spjobs.del_name
		,spjobs.job_ref
		,spjobs.cus_ref
		,FORMAT(DATEADD(hh, DATEDIFF(hh, GETUTCDATE(), GETDATE()), lc.gps_time) ,'dd-MM-yyyy HH:mm:ss')											
														as 'event_datetime_sys'							-- Event Date/Time SYS
		,FORMAT(lc.gps_time ,'dd-MM-yyyy HH:mm:ss')			as 'event_datetime_utc'							-- Event Date/Time UTC
		,lc.job_reference									as 'sapphire_manifest'
	FROM 
		SapphireSQL.FTS.spjobs spjobs
	JOIN JITTERBIT_EVENTSTORE.SAPPHIRE.runs_xref runs_xref 
		ON (LTRIM(spjobs.job_no) = runs_xref.job_number)
	LEFT JOIN MTDATA_KINESIS.dbo.CW_Leg_Complete lc 
		ON (runs_xref.mtdata_dispatch_id = lc.dispatch_id AND runs_xref.leg_id = lc.leg_id)
	WHERE
		spjobs.col_code LIKE 'CW%'
		AND (runs_xref.leg_type = 'DL')																-- Filter only Delivery Legs
		AND (
			-- GPS time is converted to local time by getting the difference in hours between the current UTC time and the current local time
			-- This isn't historically precise and will be up to 1 hour off on daylight savings changes but the potential for difference
			-- will last only 1 day at the two DST time changes during a year.
			-- SQL 2016 introduced an AT TIMEZONE function to adjust a historical date from one timezone to another
			-- Until this query can be run on a newer version of SQL Server, this imprecise approach is used
			-- In addition, this does not currently take into account the timezone where the action was performed
			DATEADD(hh, DATEDIFF(hh, GETUTCDATE(), GETDATE()), lc.gps_time) >= DATEADD(day, DATEDIFF(day, 0, GETDATE()) - 1, 0)  
			AND DATEADD(hh, DATEDIFF(hh, GETUTCDATE(), GETDATE()), lc.gps_time) < DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0)  
			)
),
cte_edi_ref AS(

  	SELECT 
		cw_hist.itemlinedescription				AS 'barcode'
		,cw_hist.customerreference				AS 'SO'
		,cw_hist.loadnumber						AS 'LDN'
		,cw_hist.sourcecol						AS 'SF'
		,LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(cw_hist.Collect_Name
									  ,CHAR(10),  CHAR(32))
									  ,CHAR(13),  CHAR(32))
									  ,CHAR(160), CHAR(32))
									  ,CHAR(9),   CHAR(32))))					AS 'SF_name'
		,cw_hist.sourcedel						AS 'ST'
		,LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(cw_hist.Deliver_Name
									  ,CHAR(10),  CHAR(32))
									  ,CHAR(13),  CHAR(32))
									  ,CHAR(160), CHAR(32))
									  ,CHAR(9),   CHAR(32))))					AS 'ST_name'
	FROM 
		SapphireSQL.dbo.chemistoutput_table_history cw_hist
	WHERE
		cw_hist.editprocesseddatetime = (
			SELECT
			  MAX(editprocesseddatetime)
			FROM
			  SapphireSQL.dbo.chemistoutput_table_history latest
			WHERE
			  cw_hist.itemlinedescription = latest.itemlinedescription
		  )
)
,cte_source_item_ref AS(
	SELECT 
		rod.item_reference				as 'barcode'
		,rod.delivery_customer				as 'target_customer_code'
		,MAX(rxref.mtdata_dispatch_id)		as 'target_dispatch_id'
	FROM 
		JITTERBIT_EVENTSTORE.SAPPHIRE.runs_out_detail rod
	LEFT JOIN 
		(SELECT DISTINCT job_number, mtdata_dispatch_id FROM JITTERBIT_EVENTSTORE.SAPPHIRE.runs_xref) rxref 
		ON rod.job_number = rxref.job_number
	GROUP BY rod.item_reference,rod.delivery_customer	
)
-- OUTER SELECT TO CATEGORIZE AND FIND THE TARGET OF ISSUE ITEMS
SELECT																								
	result.dispatch_id
	,result.sapphire_manifest
	,result.event_datetime_sys
	,result.event_datetime_utc
	,result.customer_state
	,result.customer_code
	,result.customer_name
	,result.barcode
	,result.scanned_timestamp_utc
	,result.scanned_at_location_code
	,result.scanned_at_location_name
	,result.result
	,CASE
		WHEN result.barcode = result.customer_code
			THEN 'Destination store code'
		WHEN result = 'Excess' AND result.scanned_at_location_code != cte_edi_ref.ST  AND result.dispatch_id != cte_source_item_ref.target_dispatch_id
			THEN 'Excess - Misallocated. Destination store "' + cte_edi_ref.ST_name + ' ('+cte_edi_ref.ST+')" via dispatch id '+cte_source_item_ref.target_dispatch_id+'.' 
		WHEN result = 'Excess' AND result.scanned_at_location_code != cte_edi_ref.ST
			THEN 'Excess - Misallocated. Destination store "' + cte_edi_ref.ST_name + ' ('+cte_edi_ref.ST+')" but no dispatch assigned.' 
		WHEN result = 'Excess' AND result.scanned_at_location_code = cte_edi_ref.ST AND result.dispatch_id != cte_source_item_ref.target_dispatch_id
			THEN 'Excess - Correct destination but assigned on different dispatch id '+cte_source_item_ref.target_dispatch_id+'.'
		WHEN result = 'Excess' AND result.scanned_at_location_code = cte_edi_ref.ST
			THEN 'Excess - Correct destination but no dispatch assigned.' 
	END																								AS 'error_message'			-- 
	,cte_edi_ref.SO
	,cte_edi_ref.LDN
	,cte_edi_ref.SF_name
	,cte_edi_ref.SF
	,cte_edi_ref.ST
	,cte_edi_ref.ST_name
FROM
(
	-- INNER SELECT TO FIND MISSING/EXCESS ITEMS
	SELECT
		COALESCE(NULLIF(expect.dispatch_id,''),NULLIF(actual.dispatch_id,''))							AS 'dispatch_id'
		,COALESCE(NULLIF(expect.sapphire_manifest,''),NULLIF(actual.sapphire_manifest,''))				AS 'sapphire_manifest'
		,COALESCE(NULLIF(expect.event_datetime_sys,''),NULLIF(actual.event_datetime_sys,''))			AS 'event_datetime_sys'
		,COALESCE(NULLIF(expect.event_datetime_utc,''),NULLIF(actual.event_datetime_utc,''))			AS 'event_datetime_utc'
		,COALESCE(NULLIF(expect.del_state,''),NULLIF(actual.del_state,''))								AS 'customer_state'
		,COALESCE(NULLIF(expect.delivery_customer,''),NULLIF(actual.delivery_customer,''))				AS 'customer_code'
		,COALESCE(NULLIF(expect.leg_title,''),NULLIF(actual.leg_title,''))								AS 'customer_name'
		,LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(COALESCE(NULLIF(expect.item_external_reference,''),NULLIF(actual.item_external_reference,'')) 
									  ,CHAR(10),  CHAR(32))
									  ,CHAR(13),  CHAR(32))
									  ,CHAR(160), CHAR(32))
									  ,CHAR(9),   CHAR(32))))
																										AS 'barcode'		-- Item Barcode
		,CASE
				WHEN expect.item_external_reference = actual.item_external_reference 
					THEN 'OK'
				WHEN expect.item_external_reference IS NOT NULL AND actual.item_external_reference IS NULL 
					THEN 'Short'
				ELSE 'Excess'
			END																							AS 'result'			-- Result OK/Short/Excess
		,FORMAT(actual.timestamp ,'dd-MM-yyyy HH:mm:ss')												AS 'scanned_timestamp_utc'
		,actual.del_code																				AS 'scanned_at_location_code'
		,actual.del_name																				AS 'scanned_at_location_name'
	FROM 
		(SELECT cte_ytd.*, lci.item_external_reference FROM cte_ytd_dispatch cte_ytd LEFT JOIN MTDATA_KINESIS.dbo.CW_Leg_Complete_Items lci ON (cte_ytd.sequence_id = lci.sequence_id)) expect
	FULL OUTER JOIN 
		(SELECT DISTINCT cte_ytd.*, lcai.item_external_reference, lcai.timestamp FROM cte_ytd_dispatch cte_ytd JOIN MTDATA_KINESIS.dbo.CW_Leg_Complete_Action_Items lcai 
			ON (cte_ytd.sequence_id = lcai.sequence_id 
				AND lcai.item_count > 0 
				AND lcai.item_external_reference IS NOT NULL)) actual
		ON (expect.dispatch_id = actual.dispatch_id
		AND expect.sequence_id = actual.sequence_id
		AND expect.item_external_reference = actual.item_external_reference)
	) result
LEFT JOIN cte_edi_ref ON (cte_edi_ref.barcode = result.barcode)
LEFT JOIN cte_source_item_ref ON (cte_source_item_ref.barcode = result.barcode AND cte_source_item_ref.target_customer_code = cte_edi_ref.ST)
ORDER BY result.customer_state, result.customer_code, result.result, result.barcode

	
