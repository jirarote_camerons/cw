#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import logging
import base64
import requests

# Enable to see the log within 
log_format = '%(asctime)s  %(name)s  %(levelname)s: %(message)s'
logging.basicConfig(format=log_format, level=logging.INFO)


# In[2]:


class oauth2_handler:
    logger = logging.getLogger('oauth2_handler')
    
    token_obj = None
    additional_params = None
    
    oauth_services = {
        "microsoft" : "https://login.microsoftonline.com/common/oauth2/token"
    }
    
    def __init__(self, 
                 username = None, password = None, is_base64_password = True, service="microsoft"):
        self.username = username
        self.password = password
        self.is_base64_password = is_base64_password
        self.service = service
        
    ###################  CONNECTION MANAGER  ###########################
    def open_connection(self, params=None):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {}".format(func_name))
        
        if(self.service == "microsoft"):
            data = '''
                grant_type=password
                &username={username}
                &password={password}
                &client_id={client_id}
                &client_secret={client_secret}
                &resource=https://analysis.windows.net/powerbi/api
                &tenant_id={tenant_id}
            '''.format(username = self.username,
                       password = base64.b64decode(self.password).decode("utf-8") if self.is_base64_password else self.password,
                       client_id = params['client_id'],
                       client_secret = params['client_secret'],
                       tenant_id = params['tenant_id']).strip().replace('\n', '').replace(' ', '')
            
            headers = {'Content-Type': 'application/x-www-form-urlencoded'}
            oauth_url = "https://login.microsoftonline.com/common/oauth2/token"
            response = requests.post(oauth_url,data=data, headers=headers)
            
            self.logger.info("open_connection response code: {}".format(response.status_code))
            
            if(response.status_code == 200):
                self.token_obj = response.json()
                self.additional_params = params
        else:
            self.logger.info("No services matched your selection : {}",self.service)
        
        
    def refresh_token(self):
        if(self.token_obj == None):
            self.logger.info("need to open_connection first!!")
            return 
        
        if(self.service == "microsoft"):
            data = '''
                grant_type=refresh_token
                &refresh_token={refresh_token}
                &client_id={client_id}
                &client_secret={client_secret}
                &resource=https://analysis.windows.net/powerbi/api
                &tenant_id={tenant_id}
            '''.format( refresh_token = self.token_obj['refresh_token'],
                        client_id = self.additional_params['client_id'],
                       client_secret = self.additional_params['client_secret'],
                       tenant_id = self.additional_params['tenant_id']).strip().replace('\n', '').replace(' ', '')
            
            headers = {'Content-Type': 'application/x-www-form-urlencoded'}
            oauth_url = "https://login.microsoftonline.com/common/oauth2/token"
            response = requests.post(oauth_url,data=data, headers=headers)
            
            self.logger.info("refresh_token response code: {}".format(response.status_code))
            
            if(response.status_code == 200):
                self.token_obj = response.json()
        else:
            self.logger.info("No services matched your selection : {}",self.service)
            print(token_obj)
            
            


# In[3]:


# temp = oauth2_handler("Camerons.bi.prod@camerons.com.au", "Dhf6eUFt**pRp-", False)


# In[4]:


# temp.open_connection(params={"tenant_id":"62e77b65-9d5a-4eb5-957d-28648501225d",
#                             "client_id":"c5eebab6-22f6-4d6f-a81a-5f0c50b6eb46",
#                             "client_secret":"3e-8Q~nhjEp~LEtnbvJVuPGRFYDqBornYt9hAcpj"})


# In[5]:


# print(temp.token_obj['refresh_token'])


# In[6]:


# temp.refresh_token()


# In[ ]:




