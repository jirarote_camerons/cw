#!/usr/bin/env python
# coding: utf-8

# In[12]:


import logging
import sqlalchemy as sa
import sys
import pandas as pd

# Enable to see the log within 
# log_format = '%(asctime)s  %(name)s  %(levelname)s: %(message)s'
# logging.basicConfig(format=log_format, level=logging.INFO)


class dbconnection:
    logger = logging.getLogger('dbconnection')
    engine = None
    
    def __init__(self, 
                 servername, database = 'MASTER', 
                 username = None, password = None,
                 dbdriver = 'ODBC Driver 11 for SQL Server'):
        self.servername = servername
        self.database = database
        self.username = username
        self.password = password
        self.dbdriver = dbdriver
    
    
    ###################  CONNECTION MANAGER  ###########################
    def open_sqlconnection(self):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {}".format(func_name))
        try:
            self.logger.info("Connecting to: [{}].[{}]".format(self.servername, self.database))
            # create the connection
            if(self.username == None):
                self.logger.info("Database Authen: Windows Authentication")
                connection_uri = sa.engine.URL.create(
                    "mssql+pyodbc",
                    host = self.servername,
                    database = self.database,
                    query={"driver": self.dbdriver },
                )
            else:

                self.logger.info("Database Authen: SQL Server Authentication")
                connection_uri = sa.engine.URL.create(
                    "mssql+pyodbc",
                    username = self.username,
                    password = self.password,
                    host = self.servername,
                    database = self.database,
                    query={"driver": self.dbdriver },
                )
    
            engine = sa.create_engine(connection_uri)
            res = engine.connect()
            self.logger.info('Database connected!')
            self.engine = engine
            return res
        except Exception as e:
            self.logger.error('Database connection failed...')
            raise Exception(e)
            
    def close_sqlconnection(self):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {}".format(func_name))

        try:
            self.engine.dispose()
            self.logger.info('Database disconnected!')
        except Exception as e:
            self.logger.error('Database disconnection failed...')
            raise Exception(e)    
            
    ###################  CONNECTION MANAGER  ###########################
    
    ###################  DATABASE QUERY  ###########################
    def is_db_table_existed(self, table_name, schema = 'dbo'):
        isTable = sa.inspect(self.engine).has_table(table_name, schema = schema)  
        self.logger.debug("Table '[{}].[{}].[{}].[{}]' {}".format(
            self.servername, self.database, schema, table_name, 'existed' if isTable else 'does not exist'))
        return isTable
    
    def read_to_df(self, table_name, schema = 'dbo', n = None):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {}".format(func_name))

        sql = """
            SELECT {} *
                FROM [{}].[{}]
        """.format("" if n == None else "TOP ({})".format(n), schema, table_name)
            
        try:
            self.logger.debug('====== SQL ======')
            self.logger.debug(sql)
            self.logger.debug('=================')
            query_df = pd.read_sql(sql, self.engine)
            self.logger.info('Read Success with df size: {}'.format(query_df.shape))
            return query_df
        except Exception as e:
            self.logger.error('Read failed...')
#             raise Exception(e)  
            
    def write_from_df(self, table_name, input_df, schema = 'dbo', is_replace = False, is_admin = False):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {} {}".format(func_name,table_name))
    
        if(self.is_db_table_existed(table_name , schema)):
            try:
                if(is_replace):
                    logger.debug("Truncate Table: [{}].[{}]".format(schema, table_name))
                    # If table exist -> Truncate before add value
                    Session = sa_orm.sessionmaker(bind=self.engine)
                    session = Session()
                    session.execute('''TRUNCATE TABLE [{}].[{}]'''.format(schema, table_name))
                    session.commit()
                    session.close()
                    self.logger.info('Truncate Table success')
                
                input_df.to_sql(table_name, index=False, con=self.engine, schema = schema, if_exists="append")
                self.logger.info('Write data to table success')
            except Exception as e:
                self.logger.error('Write data to table failed')
                raise Exception(e)  
        else:
            if(is_admin):
                self.logger.warning("Creating new table should be done manually...")
                self.logger.warning("OVERRIDE Creating table as ADMIN")
                try:
                    input_df.to_sql(table_name, index=False, con=self.engine, schema = schema, if_exists="append")
                    self.logger.info('Creating new table success')
                    self.logger.info('Write data to table success')
                except Exception as e:
                    self.logger.error('Creating new table failed')
                    raise Exception(e)                       
            else:
                self.logger.error("Write data to table failed. Table does not exist")
    
    def read_sql(self, sql_script_string):
        try:
            self.logger.debug('====== SQL ======')
            self.logger.debug(sql_script_string)
            self.logger.debug('=================')
            query_df = pd.read_sql(sql_script_string,self.engine) 
            self.logger.info('Read Success with df size: {}'.format(query_df.shape))
            return query_df
        except Exception as e:
            self.logger.error('Read failed...')
    
    def read_sql_file(self, script_file_path):
        try:
            # Open the external sql file.
            file = open(script_file_path, 'r')

            # Read out the sql script text in the file.
            sql_script_string = file.read()

            # Close the sql file object.
            file.close()
            self.logger.info('Open SQL file success')
            
            return self.read_sql(sql_script_string)
        except Exception as e:
            self.logger.error('Open SQL file failed...')
            

    def execute_sql(self, sql_script_string):
        try:
            self.logger.debug('====== SQL ======')
            self.logger.debug(sql_script_string)
            self.logger.debug('=================')
            Session = sa_orm.sessionmaker(bind=self.engine)
            session = Session()
            session.execute(sql_script_string)
            session.commit()
            session.close()
            self.logger.info('Execute SQL success')
        except Exception as e:
            self.logger.error('Execute SQL failed...')
            
    def execute_sql_file(self, script_file_path):
        try:
            # Open the external sql file.
            file = open(script_file_path, 'r')

            # Read out the sql script text in the file.
            sql_script_string = file.read()

            # Close the sql file object.
            file.close()
            self.logger.info('Open SQL file success')
            
            self.execute_sql(sql_script_string)
        except Exception as e:
            self.logger.error('Open SQL file failed...')

    ###################  DATABASE QUERY  ###########################
    
    


# In[13]:


if __name__ == "__main__":
    print ("Executed when invoked directly")

    # Testing Class
    # test = dbconnection('VSSQLDW01')
    test = dbconnection('VSDEVODS01','BI')
    test.open_sqlconnection()
    print(test.engine)
    print(test.is_db_table_existed('script_execution','LOG'))
    print(test.read_to_df('script_executions','LOG', n = 500))
    timestamp_df = pd.DataFrame([{"script":'JJ_Test',
                                      "refresh_time":pd.Timestamp.today(),
                                      "status": "Success"}])

    test.write_from_df("script_execution", timestamp_df, schema = "LOG")
    test.close_sqlconnection()


# In[ ]:




