#!/usr/bin/env python
# coding: utf-8

# In[1]:


# setup_logger.py
import logging

log_format = '%(asctime)s  %(name)s  %(levelname)s: %(message)s'
formatter = logging.Formatter(log_format)
logging.basicConfig(format=log_format, level=logging.INFO)
logger = logging.getLogger()

logger_local = logging.getLogger('setup_logger')
logger_local.info("Initialize Logger")


# In[2]:


def set_log_file(file_fullpath):
    # add local logger
    _fileHandler = logging.FileHandler(file_fullpath)
    _fileHandler.setFormatter(formatter)
    logger.addHandler(_fileHandler)
    
    logger_local = logging.getLogger('setup_logger')
    logger_local.info("Log FileHandler Path: {}".format(file_fullpath))


# In[ ]:




