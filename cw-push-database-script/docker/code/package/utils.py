#!/usr/bin/env python
# coding: utf-8

# In[13]:


# utils.py
from pathlib import Path
import os
import logging

logger_local = logging.getLogger('utils')


# In[16]:


def path_handler(full_path):
    _path = Path(full_path)
    
    if(_path.is_file()):
        logger_local.warning("File Existed: {}".format(_path))
        return _path.resolve()
    elif(_path.is_dir()):
        logger_local.info("Directory Existed: {}".format(_path))
        raise Exception('This is not a file')
    else:
        if not Path.exists(_path.parents[0]):
            logger_local.warning("Directory not found, creating new directory: {}".format(_path.parents[0]))
            os.makedirs(_path.parents[0])
            return _path.resolve()
        else:
            return _path.resolve()


# In[17]:


def get_pytz_timezone(state):
    if(state == "VIC"):
        return 'Australia/Melbourne'
    elif(state == "QLD"):
        return 'Australia/Brisbane'
    elif(state == "NSW"):
        return 'Australia/Sydney'
    elif(state == "NT"):
        return 'Australia/Darwin'
    elif(state == "SA"):
        return 'Australia/Adelaide'
    elif(state == "TAS"):
        return 'Australia/Hobart'
    elif(state == "WA"):
        return 'Australia/Perth'
    else:
        return 'utc'


# In[ ]:




