#!/usr/bin/env python
# coding: utf-8

# In[5]:


import logging
import sys
import requests

# Enable to see the log within 
# log_format = '%(asctime)s  %(name)s  %(levelname)s: %(message)s'
# logging.basicConfig(format=log_format, level=logging.INFO)

class oauth2_handler:
    logger = logging.getLogger('oauth2_handler')
    
    token_obj = None
    
    oauth_services = {
        "microsoft" : "https://login.microsoftonline.com/common/oauth2/token"
    }
    
    def __init__(self, 
                 username = None, password = None, service="microsoft", params = None):
        self.username = username
        self.password = password
        self.service = service
        self.params = params
        
    def __enter__(self):
        return self.open_connection()
     
    def __exit__(self, exc_type, exc_value, exc_traceback):
        pass # no need to close connection because token will expired
  
        
    ###################  CONNECTION MANAGER  ###########################
    def open_connection(self):
        # Print Function Name
        func_name = sys._getframe().f_code.co_name
        self.logger.debug("Function called: {}".format(func_name))
        
        
        str_logging = "OAuth for '{service}' using '{username}'".format(
            service = self.service,
            username = self.username
        )
        

        if(self.service == "microsoft"):
            data = '''
                    grant_type=password
                    &username={username}
                    &password={password}
                    &client_id={client_id}
                    &client_secret={client_secret}
                    &resource={resource}
                    &tenant_id={tenant_id}
                '''.format(username = self.username,
                           password = self.password,
                           client_id = self.params['client_id'],
                           client_secret = self.params['client_secret'],
                           resource = self.params['resource'],
                           tenant_id = self.params['tenant_id'])

            headers = {'Content-Type': 'application/x-www-form-urlencoded'}
            oauth_url = "https://login.microsoftonline.com/common/oauth2/token"
            response = requests.post(oauth_url,data=data, headers=headers)

            self.logger.debug("open_connection response code: {}".format(response.status_code))

            if(response.status_code == 200):
                self.token_obj = response.json()
                self.logger.info(str_logging + " Authorized!")
            else:
                error_msg = "Response status code : {}".format(response.status_code)
                self.logger.debug(error_msg)
                self.logger.error(str_logging + " Unauthorized!")
                raise Exception(response.json())

            return self
        else:
            error_msg = "No services matched your selection : {}".format(self.service)
            self.logger.error(error_msg)
            raise Exception(error_msg)
        
        
    def refresh_token(self):
        if(self.token_obj == None):
            error_msg ="need to open_connection first!!"
            self.logger.error(error_msg)
            raise Exception(error_msg) 
        
        try:
            if(self.service == "microsoft"):
                data = '''
                    grant_type=refresh_token
                    &refresh_token={refresh_token}
                    &client_id={client_id}
                    &client_secret={client_secret}
                    &resource=https://analysis.windows.net/powerbi/api
                    &tenant_id={tenant_id}
                '''.format( refresh_token = self.token_obj['refresh_token'],
                            client_id = self.params['client_id'],
                            client_secret = self.params['client_secret'],
                            resource = self.params['resource'],
                            tenant_id = self.params['tenant_id'])

                headers = {'Content-Type': 'application/x-www-form-urlencoded'}
                oauth_url = "https://login.microsoftonline.com/common/oauth2/token"
                response = requests.post(oauth_url,data=data, headers=headers)

                self.logger.debug("refresh_token response code: {}".format(response.status_code))

                if(response.status_code == 200):
                    self.token_obj = response.json()
                else:
                    error_msg = "Response status code : {}".format(response.status_code)
                    self.logger.error(error_msg)
                    raise Exception(response.json())
            else:
                error_msg = "No services matched your selection : {}".format(self.service)
                self.logger.error(error_msg)
                raise Exception(error_msg)
                
        except Exception as e:
            self.logger.error(str_logging + " Unauthorized!")
            self.logger.debug(e)
            
            raise Exception(e)
            
    ############################## Additional Parameters ##################################        
    def get_microsoft_resource(key):
        microsoft_resources = {
            "powerbi" : "https://analysis.windows.net/powerbi/api",
            "graphapi" : "https://graph.microsoft.com"
        }
        return microsoft_resources[key]


# In[8]:


# Testing
if __name__ == "__main__":
    with oauth2_handler(username = "Camerons.bi.prod@camerons.com.au", 
                          password = "",
                          service="microsoft",
                          params={"tenant_id":"62e77b65-9d5a-4eb5-957d-28648501225d",
                                "client_id":"c5eebab6-22f6-4d6f-a81a-5f0c50b6eb46",
                                "client_secret":"",
                                 "resource":oauth2_handler.get_microsoft_resource("powerbi")}) as my_oauth:
        
        my_oauth.token_obj['refresh_token']
        print(my_oauth.token_obj['refresh_token'])
        my_oauth.refresh_token()


# In[ ]:




