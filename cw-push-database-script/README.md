Deploying Steps

#########  local machine
cd C:\Users\jjirasirikul\Documents\BitBucket\Camerons_CW\cw-push-database-script
docker-compose up -d --build
docker save -o cw-push-database-script.tar cw-push-database-script_main
docker-compose stop

COPY FILE TO SERVER D:\DockerImages\cw-push-database-script (via T:\ Drive is fastest)
mv cw-push-database-script.tar T:\BIScript_data\cw-push-database-script.tar

#########  server (since no compose component on the server)
# Remark -  You may need to restart "Hyper-V Virtual Machine Management" service before deploy

cd D:\DockerImages\cw-push-database-script
docker load -i .\cw-push-database-script.tar
docker-compose up -d
docker logs cw-push-database-script-dev-1
docker logs cw-push-database-script-prod-1


##########  BACKUP
# docker rm -f cw-push-database-script_dev
# docker run --platform linux/amd64 --name cw-push-database-script_dev --env-file .env\dev.env -d --restart=always cw-push-database-script_main


